from yt_dlp import YoutubeDL


def download_audio_by_url( video_url: str, download_path: str) -> str:
    """Method for downloading the audio from a youtube video 
        
    Args:
        url (str): The url for the video whoose audio is to be downloaded
        download_path (str): The path of the audio file to be downloaded.

    Returns:
        file_name (str): The downloaded file name in the path provided.
    """
    
    ydl_opts = {
    'outtmpl': f'{download_path}/%(title)s.%(ext)s',
    'format': 'bestaudio/best',
    'keepvideo': True,
    'extract_audio': True,
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '240',
    }],
    'restrictfilenames': True,
    'retries': 10
    }
    
    with YoutubeDL(ydl_opts) as ydl:
        # Extract info will download the video + mp3
        info_dict = ydl.extract_info(video_url, download = True)
        download_path = ydl.prepare_filename(info_dict)

        return download_path[0:-5] + ".mp3"
