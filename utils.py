from logging import Logger, Formatter, StreamHandler, getLogger, DEBUG
import sys

def get_logger(name: str) -> Logger:
   
    formatter = Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
   
    screen_handler = StreamHandler(stream=sys.stdout)
    screen_handler.setFormatter(formatter)
    logger = getLogger(name)
    logger.propagate = False
    logger.setLevel(DEBUG)
    logger.addHandler(screen_handler)
    return logger
