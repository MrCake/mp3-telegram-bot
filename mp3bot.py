from telebot.async_telebot import AsyncTeleBot
from telebot.types import Message
from youtube_wrapper import download_audio_by_url
from loguru import logger
import asyncio
import traceback
import time
import mp3bot_strings
import os


class mp3bot():
    
    __bot = None 
    __audio_path = "./audios"
    log = None
    
    __max_download_retries = 3
    __delay_between_retry_s = 10

    def __init__(self, TOKEN) -> None:
        self.log = logger
        os.makedirs(self.__audio_path, exist_ok= True)
        self.__bot = AsyncTeleBot(TOKEN)




    def run(self) -> None:
        self.__bot.set_update_listener(self.__listener)
        self.define_commands()
        self.log.info("-- Bot has been initialized --")
        asyncio.run(self.__bot.infinity_polling(skip_pending = True))
        




    def define_commands(self) -> None:

        @self.__bot.message_handler(commands=['help', 'start'])
        async def _(message):
            """ Send help / initialization message """
            await self.__bot.reply_to(message, mp3bot_strings.help)
    

    # Extract the first youtube url from a message
    def __extract_url_from_message(self, message : str) -> str:
        url = ""
        # Isolate YouTube URL from the message
        for portion in message.split():
            if "youtube.com/watch?" in portion or "youtube.com/playlist?" in portion or "youtu.be/" in portion:
                url = portion

        return url
    

            

    # Listener for chat messages
    async def __listener(self, messages: list[Message] ) -> None:
        try:

            for message in messages:

                # Check if in the message there is present a youtube url
                if message.reply_to_message is not None and ("youtube.com/watch?" in message.reply_to_message.text.lower() or "youtube.com/playlist?" in  message.reply_to_message.text.lower() or "youtu.be/" in message.reply_to_message.text.lower()):
                    
                    # Download an individual youtube video as mp3
                    if "!d" in message.text.lower():
                        
                        try:
                            video_url = self.__extract_url_from_message( str(message.reply_to_message.text) )

                            
                            if "?list" in video_url or "&list" in video_url:
                                video_url = video_url.split("list")[0]

                            message_wait_for_download = await self.__bot.reply_to(message, mp3bot_strings.wait_for_download_text)
                            download_path = download_audio_by_url(video_url, self.__audio_path)
                            await self.__bot.delete_message(chat_id= message_wait_for_download.chat.id, message_id= message_wait_for_download.id )

                            # Send audio
                            audio = open( download_path , "rb")
                            await self.__bot.send_audio(chat_id= message.chat.id, audio=audio)
                            audio.close()

                            self.log.info("Audio with path " + download_path + " succesfully sent")

                        except Exception as e:
                            self.log.error( "Error on !d" + traceback.format_exc() )
                            await self.__bot.reply_to(message, mp3bot_strings.cant_convert)


                        



        except Exception as e:
            self.log.info( traceback.format_exc() )

